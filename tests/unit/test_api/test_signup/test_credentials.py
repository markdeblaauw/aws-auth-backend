import pytest

from api.runtime.signup.app import Credentials


def test_correct_password():
    correct_password = "12Abcde!"

    output_value = Credentials.check_password(value=correct_password)

    assert output_value == correct_password


@pytest.mark.parametrize(
    "incorrect_password",
    [
        "12Abce!",
        "12Abcde!" * 4 + "1",
        "12Abcde",
        "12abcde!",
        "12ABCDE!",
    ],
)
def test_incorrect_password_errors(incorrect_password):
    with pytest.raises(ValueError) as info:
        Credentials.check_password(incorrect_password)
