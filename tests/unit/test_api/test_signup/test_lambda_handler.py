import json
from unittest.mock import patch

from api.runtime.signup.app import handler


def test_lambda_handler_200_response():
    with patch("api.runtime.signup.app.signup") as mock_signup:
        mock_signup.return_value = {"status_code": 200}

        response = handler(
            event={
                "path": "/api/sign-up",
                "httpMethod": "POST",
                "body": json.dumps({"username": "mark@gmail.com", "password": "1mMark!blaauw"}),
            },
            context={},
        )

        assert response == {"status_code": 200}
        mock_signup.assert_called_once_with(username="mark@gmail.com", password="1mMark!blaauw")


def test_lambda_handler_invalid_password():
    with patch("api.runtime.signup.app.signup") as mock_signup:
        mock_signup.return_value = {"status_code": 200}

        response = handler(
            event={
                "path": "/api/sign-up",
                "httpMethod": "POST",
                "body": json.dumps({"username": "mark@gmail.com", "password": "mark"}),
            },
            context={},
        )

        assert response == {
            "isBase64Encoded": False,
            "statusCode": 400,
            "body": json.dumps({"message": "Invalid password"}),
            "headers": {
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        }

        mock_signup.call_count == 0


def test_lambda_handler_invalid_email():
    with patch("api.runtime.signup.app.signup") as mock_signup:
        mock_signup.return_value = {"status_code": 200}

        response = handler(
            event={
                "path": "/api/sign-up",
                "httpMethod": "POST",
                "body": json.dumps({"username": "markgmail.com", "password": "213Mar!aseas"}),
            },
            context={},
        )

        assert response == {
            "isBase64Encoded": False,
            "statusCode": 400,
            "body": json.dumps({"message": "Invalid email"}),
            "headers": {
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        }

        assert mock_signup.call_count == 0
