import json
from unittest.mock import patch

import pytest

from api.runtime.signup.app import signup


@pytest.fixture()
def created_client_pool_with_secret(cognito_client, secretsmanager_client):
    pool_response = cognito_client.create_user_pool(
        PoolName="testing",
        Policies={
            "PasswordPolicy": {
                "MinimumLength": 8,
                "RequireUppercase": True,
                "RequireLowercase": True,
                "RequireNumbers": True,
                "RequireSymbols": True,
                "TemporaryPasswordValidityDays": 123,
            }
        },
    )

    user_pool_id = pool_response["UserPool"]["Id"]

    pool_client = cognito_client.create_user_pool_client(
        UserPoolId=user_pool_id,
        ClientName="testing2",
        GenerateSecret=True,
        ExplicitAuthFlows=["ALLOW_ADMIN_USER_PASSWORD_AUTH"],
    )

    client_id = pool_client["UserPoolClient"]["ClientId"]
    secret_string = json.dumps(
        {
            "userPoolId": user_pool_id,
            "clientId": client_id,
            "clientSecret": pool_client["UserPoolClient"]["ClientSecret"],
        }
    )
    secret_name = "signup_testing"

    secretsmanager_client.create_secret(Name=secret_name, SecretString=secret_string)

    return {"client_id": client_id, "secret_name": secret_name, "secret_string": secret_string}


def test_signup_200_response(created_client_pool_with_secret):
    with patch.dict(
        "api.runtime.signup.app.os.environ",
        {
            "secret_name": created_client_pool_with_secret["secret_name"],
            "client_id": created_client_pool_with_secret["client_id"],
        },
    ):
        response = signup(username="mark@dummy.nl", password="markdeblaauw")

        assert response["statusCode"] == 200
        assert json.loads(response["body"]) == {"message": "signup succesful"}


def test_signup_missing_the_secret_key(created_client_pool_with_secret):
    with patch.dict(
        "api.runtime.signup.app.os.environ",
        {"secret_name": "no_name", "client_id": created_client_pool_with_secret["client_id"]},
    ):
        response = signup(username="mark@dummy.nl", password="markdeblaauw")

        assert response["statusCode"] == 500


def test_signup_username_already_exists(created_client_pool_with_secret):
    with patch.dict(
        "api.runtime.signup.app.os.environ",
        {
            "secret_name": created_client_pool_with_secret["secret_name"],
            "client_id": created_client_pool_with_secret["client_id"],
        },
    ):
        response1 = signup(username="mark@dummy.nl", password="markdeblaauw")

        response2 = signup(username="mark@dummy.nl", password="markdeblaauw")

        assert response1["statusCode"] == 200
        assert json.loads(response1["body"]) == {"message": "signup succesful"}

        assert response2["statusCode"] == 400
        assert json.loads(response2["body"]) == {"message": "This username already exists"}
