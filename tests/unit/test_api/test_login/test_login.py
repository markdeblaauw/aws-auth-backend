import json
from unittest.mock import patch

import pytest

from api.runtime.login.app import login


@pytest.fixture()
def created_client_pool_with_secret(cognito_client, secretsmanager_client):
    pool_response = cognito_client.create_user_pool(
        PoolName="testing",
        Policies={
            "PasswordPolicy": {
                "MinimumLength": 8,
                "RequireUppercase": True,
                "RequireLowercase": True,
                "RequireNumbers": True,
                "RequireSymbols": True,
                "TemporaryPasswordValidityDays": 123,
            }
        },
    )

    user_pool_id = pool_response["UserPool"]["Id"]

    pool_client = cognito_client.create_user_pool_client(
        UserPoolId=user_pool_id,
        ClientName="testing2",
        GenerateSecret=True,
        ExplicitAuthFlows=["ALLOW_ADMIN_USER_PASSWORD_AUTH"],
    )

    client_id = pool_client["UserPoolClient"]["ClientId"]
    secret_string = json.dumps(
        {
            "userPoolId": user_pool_id,
            "clientId": client_id,
            "clientSecret": pool_client["UserPoolClient"]["ClientSecret"],
        }
    )
    secret_name = "signup_testing"

    secretsmanager_client.create_secret(Name=secret_name, SecretString=secret_string)

    username = "dummy@gmail.com"
    not_confirmed = "not_confirmed@gmail.com"
    password = "Dummy!123"

    cognito_client.sign_up(
        ClientId=client_id,
        Username=username,
        Password=password,
    )

    cognito_client.sign_up(
        ClientId=client_id,
        Username=not_confirmed,
        Password=password,
    )

    cognito_client.confirm_sign_up(
        ClientId=client_id,
        Username=username,
        ConfirmationCode="123456",
        ForceAliasCreation=False,
    )

    return {
        "client_id": client_id,
        "secret_name": secret_name,
        "secret_string": secret_string,
        "user1": {"password": password, "username": username},
    }


def test_login_200_response(created_client_pool_with_secret):
    with patch.dict(
        "api.runtime.login.app.os.environ",
        {
            "secret_name": created_client_pool_with_secret["secret_name"],
            "client_id": created_client_pool_with_secret["client_id"],
        },
    ):
        response = login(
            username=created_client_pool_with_secret["user1"]["username"],
            password=created_client_pool_with_secret["user1"]["password"],
        )

    body = json.loads(response["body"])

    assert response["statusCode"] == 200
    assert body.get("id_token") is not None
    assert body.get("refresh_token") is not None
    assert body.get("access_token") is not None
    assert body.get("expires_in") is not None


def test_signup_missing_secret_key(created_client_pool_with_secret):
    with patch.dict(
        "api.runtime.login.app.os.environ",
        {"secret_name": "no_name", "client_id": created_client_pool_with_secret["client_id"]},
    ):
        response = login(
            username=created_client_pool_with_secret["user1"]["username"],
            password=created_client_pool_with_secret["user1"]["password"],
        )

    assert response["statusCode"] == 500


def test_username_is_incorrect(created_client_pool_with_secret):
    with patch.dict(
        "api.runtime.login.app.os.environ",
        {
            "secret_name": created_client_pool_with_secret["secret_name"],
            "client_id": created_client_pool_with_secret["client_id"],
        },
    ):
        response = login(username="unknown", password=created_client_pool_with_secret["user1"]["password"])

    assert response["statusCode"] == 400


def test_password_is_incorrect(created_client_pool_with_secret):
    with patch.dict(
        "api.runtime.login.app.os.environ",
        {
            "secret_name": created_client_pool_with_secret["secret_name"],
            "client_id": created_client_pool_with_secret["client_id"],
        },
    ):
        response = login(username=created_client_pool_with_secret["user1"]["username"], password="wrong")

    assert response["statusCode"] == 400
    assert json.loads(response["body"]) == {"message": "The username or password is incorrect"}
