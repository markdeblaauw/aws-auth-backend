import json
from unittest.mock import patch

import pytest

from api.runtime.confirm_signup.app import confirm_signup


@pytest.fixture()
def created_client_pool_with_secret(cognito_client, secretsmanager_client):
    pool_response = cognito_client.create_user_pool(
        PoolName="testing",
        Policies={
            "PasswordPolicy": {
                "MinimumLength": 8,
                "RequireUppercase": True,
                "RequireLowercase": True,
                "RequireNumbers": True,
                "RequireSymbols": True,
                "TemporaryPasswordValidityDays": 123,
            }
        },
    )

    user_pool_id = pool_response["UserPool"]["Id"]

    pool_client = cognito_client.create_user_pool_client(
        UserPoolId=user_pool_id,
        ClientName="testing2",
        GenerateSecret=True,
        ExplicitAuthFlows=["ALLOW_ADMIN_USER_PASSWORD_AUTH"],
    )

    client_id = pool_client["UserPoolClient"]["ClientId"]
    secret_string = json.dumps(
        {
            "userPoolId": user_pool_id,
            "clientId": client_id,
            "clientSecret": pool_client["UserPoolClient"]["ClientSecret"],
        }
    )
    secret_name = "signup_testing"

    secretsmanager_client.create_secret(Name=secret_name, SecretString=secret_string)

    username = "dummy"
    password = "Dummy!123"

    cognito_client.sign_up(
        ClientId=client_id,
        Username=username,
        Password=password,
    )

    return {
        "client_id": client_id,
        "secret_name": secret_name,
        "secret_string": secret_string,
        "user1": {"password": password, "username": username},
    }


def test_confirm_signup_200_response(created_client_pool_with_secret):
    with patch.dict(
        "api.runtime.confirm_signup.app.os.environ",
        {
            "secret_name": created_client_pool_with_secret["secret_name"],
            "client_id": created_client_pool_with_secret["client_id"],
        },
    ):
        response = confirm_signup(
            username=created_client_pool_with_secret["user1"]["username"], confirmation_code="123456"
        )

        assert json.loads(response["body"]) == {"message": "confirmation succesful"}


def test_confirm_signup_missing_secret_key(created_client_pool_with_secret):
    with patch.dict(
        "api.runtime.confirm_signup.app.os.environ",
        {"secret_name": "no_name", "client_id": created_client_pool_with_secret["client_id"]},
    ):
        response = confirm_signup(
            username=created_client_pool_with_secret["user1"]["username"], confirmation_code="123456"
        )

        assert response["statusCode"] == 500


def test_user_not_found(created_client_pool_with_secret):
    with patch.dict(
        "api.runtime.confirm_signup.app.os.environ",
        {
            "secret_name": created_client_pool_with_secret["secret_name"],
            "client_id": created_client_pool_with_secret["client_id"],
        },
    ):
        response = confirm_signup(username="notExistingUser", confirmation_code="123456")

    assert json.loads(response["body"]) == {"message": "Username does not exists"}
