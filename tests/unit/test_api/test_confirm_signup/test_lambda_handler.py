import json
from unittest.mock import patch

from api.runtime.confirm_signup.app import handler


def test_lambda_handler_200_response():
    with patch("api.runtime.confirm_signup.app.confirm_signup") as mock_confirm_signup:
        mock_confirm_signup.return_value = {"status_code": 200}

        response = handler(
            event={
                "path": "/api/sign-up",
                "httpMethod": "POST",
                "body": json.dumps(
                    {
                        "username": "mark@gmail.com",
                        "confirmation_code": "123456",
                    }
                ),
            },
            context={},
        )

        assert response == {"status_code": 200}
        mock_confirm_signup.assert_called_once_with(username="mark@gmail.com", confirmation_code="123456")


def test_lambda_handler_invalid_email():
    with patch("api.runtime.confirm_signup.app.confirm_signup") as mock_confirm_signup:
        mock_confirm_signup.return_value = {"status_code": 200}

        response = handler(
            event={
                "path": "/api/sign-up",
                "httpMethod": "POST",
                "body": json.dumps(
                    {
                        "username": "markgmail.com",
                        "confirmation_code": "123456",
                    }
                ),
            },
            context={},
        )

        assert response == {
            "isBase64Encoded": False,
            "statusCode": 400,
            "body": json.dumps({"message": "Invalid email"}),
            "headers": {
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        }
        assert mock_confirm_signup.call_count == 0


def test_no_confirmation_code_in_request():
    with patch("api.runtime.confirm_signup.app.confirm_signup") as mock_confirm_signup:
        mock_confirm_signup.return_value = {"status_code": 200}

        response = handler(
            event={
                "path": "/api/sign-up",
                "httpMethod": "POST",
                "body": json.dumps({"username": "mark@gmail.com", "another": "bla"}),
            },
            context={},
        )

        assert response == {
            "isBase64Encoded": False,
            "statusCode": 400,
            "body": json.dumps({"message": "Invalid request"}),
            "headers": {
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        }
        assert mock_confirm_signup.call_count == 0
