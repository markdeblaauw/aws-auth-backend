import json

import pytest

from secret.runtime.cf_secret_maker.app import create_secret, delete_secret, generate_secret_name, update_secret


@pytest.fixture()
def created_secret(secretsmanager_client):
    payload = {
        "userPoolId": "99999",
        "clientId": "zweq",
        "clientSecret": "000000",
    }

    create_secret(
        payload=payload,
        secret_name="12345-abcdef",
        stack_name="dummy",
        secret_client=secretsmanager_client,
    )

    return {"secret_payload": payload}


def test_correct_create_secret(secretsmanager_client, created_secret):
    response = secretsmanager_client.get_secret_value(SecretId="12345-abcdef")

    assert created_secret["secret_payload"] == json.loads(response["SecretString"])


def test_correct_update_secret(secretsmanager_client, created_secret):
    response = secretsmanager_client.get_secret_value(SecretId="12345-abcdef")

    assert created_secret["secret_payload"] == json.loads(response["SecretString"])

    payload_update = {
        "userPoolId": "99999",
        "clientId": "zweq",
        "clientSecret": "111111",
    }

    update_secret(
        payload=payload_update,
        secret_name="12345-abcdef",
        stack_name="dummy",
        secret_client=secretsmanager_client,
    )

    response_update = secretsmanager_client.get_secret_value(SecretId="12345-abcdef")

    assert payload_update == json.loads(response_update["SecretString"])


def test_correct_delete_secret(secretsmanager_client, created_secret):
    response = secretsmanager_client.get_secret_value(SecretId="12345-abcdef")

    assert created_secret["secret_payload"] == json.loads(response["SecretString"])

    delete_secret(secret_name="12345-abcdef", secret_client=secretsmanager_client)

    with pytest.raises(secretsmanager_client.exceptions.ResourceNotFoundException):
        secretsmanager_client.get_secret_value(SecretId="12345-abcdef")


def test_correct_generate_secret_name():
    stack_name = "abc123"
    resource_id = "123abc"

    output = generate_secret_name(stack_name=stack_name, resource_id=resource_id)

    assert len(output) == len(stack_name + resource_id) + 14

    assert bool(stack_name in output) is True
    assert bool(resource_id in output) is True
