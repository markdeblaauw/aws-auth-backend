import pytest

from lambda_layers.runtimes.lambda_utils.validators.validators import password_validator


def test_correct_password():
    correct_password = "12Abcde!"

    output_value = password_validator(password=correct_password)

    assert output_value == correct_password


@pytest.mark.parametrize(
    "incorrect_password",
    [
        "12Abce!",
        "12Abcde!" * 4 + "1",
        "12Abcde",
        "12abcde!",
        "12ABCDE!",
    ],
)
def test_incorrect_password_errors(incorrect_password):
    with pytest.raises(ValueError) as info:
        password_validator(password=incorrect_password)
