from lambda_layers.runtimes.lambda_utils.secret_hash import secret_cognito_hash


def test_secret_cognito_hash_output():
    username = "mark@dummy.com"
    client_id = "ab123"
    secret = "123ab"

    output_hash = secret_cognito_hash(
        username=username,
        cognito_client_id=client_id,
        cognito_secret=secret,
    )

    assert isinstance(output_hash, str)
