import json

from lambda_layers.runtimes.lambda_utils.api_response import APIResponseCodes, api_response


def test_correct_APIResponseCodes_values():
    assert APIResponseCodes.OK.value == 200
    assert APIResponseCodes.BAD_REQUEST.value == 400
    assert APIResponseCodes.INTERNAL_SERVER_ERROR.value == 500


def test_api_to_stringify_string_body():
    expected_response = {
        "isBase64Encoded": False,
        "statusCode": 200,
        "body": json.dumps({"message": "dummy"}),
        "headers": {"content-type": "application/json"},
    }
    response = api_response(status_code=APIResponseCodes.OK, body={"message": "dummy"})

    assert response == expected_response


def test_api_response_without_body():
    expected_response = {
        "isBase64Encoded": False,
        "statusCode": 200,
        "headers": {"content-type": "application/json"},
    }
    response = api_response(status_code=APIResponseCodes.OK)

    assert response == expected_response


def test_api_response_all_args_filled():
    expected_response = {
        "isBase64Encoded": True,
        "statusCode": 400,
        "headers": {"bla": "bla"},
        "body": json.dumps({"message": "dummy"}),
    }
    response = api_response(
        status_code=APIResponseCodes.BAD_REQUEST,
        body={"message": "dummy"},
        headers={"bla": "bla"},
        is_base_64_encoded=True,
    )

    assert response == expected_response
