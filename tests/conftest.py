import os

import boto3
import pytest
from moto import mock_cognitoidp, mock_s3, mock_secretsmanager


@pytest.fixture(scope="function")
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"
    os.environ["AWS_DEFAULT_REGION"] = "us-east-1"


@pytest.fixture(scope="function")
def s3_client(aws_credentials):
    with mock_s3():
        yield boto3.client("s3", region_name="us-east-1")


@pytest.fixture(scope="function")
def cognito_client(aws_credentials):
    with mock_cognitoidp():
        yield boto3.client("cognito-idp", region_name="us-east-1")


@pytest.fixture(scope="function")
def secretsmanager_client(aws_credentials):
    with mock_secretsmanager():
        yield boto3.client("secretsmanager", region_name="us-east-1")
