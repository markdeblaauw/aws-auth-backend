from unittest.mock import patch

import pytest

from secret.runtime.cf_secret_maker.app import handler


@pytest.fixture()
def created_client_pool(cognito_client, secretsmanager_client):
    user_pool_id = cognito_client.create_user_pool(PoolName="first")["UserPool"]["Id"]
    client_id = cognito_client.create_user_pool_client(
        UserPoolId=user_pool_id, ClientName="second", GenerateSecret=True
    )["UserPoolClient"]["ClientId"]

    create_event = {
        "RequestType": "Create",
        "ResponseURL": "dummy",
        "StackId": "asddasd/uberStack/12323",
        "ResourceType": "Custom:CustomResource",
        "LogicalResourceId": "MyCustomResource",
        "ResourceProperties": {
            "UserPoolId": user_pool_id,
            "AppClientId": client_id,
        },
    }

    return {
        "client_id": client_id,
        "event": create_event,
    }


def test_correct_handler_create_secret(created_client_pool):
    with patch("secret.runtime.cf_secret_maker.app.logging.error") as mock_logging, patch(
        "secret.runtime.cf_secret_maker.app.cfnresponse"
    ) as mock_cfnresponse:
        handler(event=created_client_pool["event"], context={})

        mock_cfnresponse.send.assert_called_once()
        mock_logging.assert_not_called()


def test_exception_handler_create_secret(created_client_pool):
    with patch("secret.runtime.cf_secret_maker.app.logging.error") as mock_logging, patch(
        "secret.runtime.cf_secret_maker.app.cfnresponse"
    ) as mock_cfnresponse:
        created_client_pool["event"]["ResourceProperties"]["UserPoolId"] = "wrong"

        handler(event=created_client_pool["event"], context={})

        mock_logging.assert_called_once()
