"""This script creates the backend."""
from typing import Any

import aws_cdk as cdk
import constructs

from api.infrastructure import API
from authentication.infrastructure import Authentication
from lambda_layers.infrastructure import LambdaLayers
from secret.infrastructure import CognitoSecret


class Backend(cdk.Stage):
    """Create the backend."""

    def __init__(self, scope: constructs.Construct, id: str, **kwargs: Any) -> None:
        """Initialise the Backend class.

        Args:
            scope (cdk.Construct): Parent construct.
            id (str): Identifier.
        """
        super().__init__(scope, id, **kwargs)

        app_name = self.node.try_get_context("application_name")
        stage = self.node.try_get_context("stage")

        # Create core application backend.
        application = cdk.Stack(self, "application", description="Backend infrastructure.")

        # Create Lambda layers.
        lambda_layers = LambdaLayers(
            application,
            "LambdaLayers",
            application_name=app_name,
            stage=stage,
        )

        # Create Cognito Authentication.
        cognito_authentication = Authentication(
            application,
            "CognitoAuthentication",
            application_name=app_name,
            stage=stage,
        )

        # Create secret and put congito secret into it.
        secret = CognitoSecret(
            application,
            "CognitoSecret",
            application_name=app_name,
            stage=stage,
            cognito_pool_id=cognito_authentication.user_pool_id,
            cognito_client_pool_id=cognito_authentication.user_pool_client_id,
        )

        # Create and deploy API
        API(
            application,
            "API",
            application_name=app_name,
            stage=stage,
            authentication=cognito_authentication,
            cognito_secret=secret,
            utils_lambda_layer=lambda_layers.utils_layer,
            powertools_layer=lambda_layers.powertools_layer,
        )
