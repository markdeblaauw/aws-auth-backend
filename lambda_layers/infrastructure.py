"""Infrastructure to create Lambda layers."""
import os
import shutil
from tempfile import TemporaryDirectory

import aws_cdk as cdk
import constructs
from aws_cdk import aws_lambda as _lambda
from aws_cdk import aws_sam as sam


class LambdaLayers(constructs.Construct):
    """Lambda layer class to create layers."""

    def __init__(
        self,
        scope: constructs.Construct,
        id: str,
        application_name: str,
        stage: str,
    ) -> None:
        """Initialises the class with additional parameters.

        Args:
            scope (constructs.Construct): Parent construct.
            id (str): Identifier.
            application_name (str): The name of the application in which this construct
                is build.
            stage (str): The stage in which this construct is part of. This can be either
                dev, test or prod.
        """
        super().__init__(scope, id)
        self.application_name = application_name
        self.stage = stage

        self._create_utils_layer()
        self._create_powertools_layer()

    def _create_utils_layer(self) -> None:
        """A Lambda layer that contains utility functions."""
        with TemporaryDirectory() as temp_dir:
            shutil.copytree(
                src=os.path.join(os.path.dirname(os.path.abspath(__file__)), "runtimes", "lambda_utils"),
                dst=os.path.join(temp_dir, "python", "lambda_utils"),
                dirs_exist_ok=True,
            )

            self.utils_layer = _lambda.LayerVersion(
                self,
                "UtilsLayer",
                code=_lambda.Code.from_asset(
                    path=temp_dir,
                ),
                compatible_runtimes=[_lambda.Runtime.PYTHON_3_8],
                description=f"A Layer that contains utility class and functions for {self.application_name}.",
                layer_version_name=f"{self.application_name}-utils-{self.stage}",
                removal_policy=cdk.RemovalPolicy.DESTROY,
            )

    def _create_powertools_layer(self) -> None:
        """A Lambda layer that contains power tools logic.

        https://awslabs.github.io/aws-lambda-powertools-python/latest/#sar
        """
        POWERTOOLS_BASE_NAME = "AWSLambdaPowertools"
        # Find latest from github.com/awslabs/aws-lambda-powertools-python/releases
        POWERTOOLS_VER = "1.25.3"
        POWERTOOLS_ARN = (
            "arn:aws:serverlessrepo:eu-west-1:057560766410:applications/aws-lambda-powertools-python-layer-extras"
        )

        powertools_app = sam.CfnApplication(
            self,
            f"{POWERTOOLS_BASE_NAME}Application",
            location={"applicationId": POWERTOOLS_ARN, "semanticVersion": POWERTOOLS_VER},
        )

        powertools_layer_arn = powertools_app.get_att("Outputs.LayerVersionArn").to_string()
        self.powertools_layer = _lambda.LayerVersion.from_layer_version_arn(
            self, f"{POWERTOOLS_BASE_NAME}", powertools_layer_arn
        )
