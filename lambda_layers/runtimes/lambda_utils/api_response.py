"""Contains standardized api response logic."""
import json
from enum import Enum
from typing import Dict, Union


class APIResponseCodes(Enum):
    """Possible API response codes."""

    OK = 200
    BAD_REQUEST = 400
    INTERNAL_SERVER_ERROR = 500


def api_response(
    status_code: APIResponseCodes,
    body: Union[str, dict] = None,
    headers: Dict[str, str] = {"content-type": "application/json"},
    is_base_64_encoded: bool = False,
) -> dict:
    """Standardise API responses.

    Args:
        status_code (APIResponseCodes): API standard server response code.
        body (Union[str, dict], optional): The body of the response.
            Defaults to None.
        headers (Dict[str, str], optional): Response headers.
            Defaults to { 'content-type': 'application/json' }.
        is_base_64_encoded (bool, optional): Whether to encode
            the response. Defaults to False.

    Returns:
        dict: An valid API response.
    """
    if isinstance(body, dict):
        body = json.dumps(body)

    if body:
        return {
            "isBase64Encoded": is_base_64_encoded,
            "statusCode": status_code.value,
            "body": body,
            "headers": headers,
        }
    else:
        return {
            "isBase64Encoded": is_base_64_encoded,
            "statusCode": status_code.value,
            "headers": headers,
        }
