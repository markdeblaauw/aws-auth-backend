"""Contains custom Pydantic validators."""
import re


def password_validator(password: str) -> str:
    """Validation for password.

    The following properties should hold:
        - Number of characters should be [8, 32]
        - Should have at least one lowercase character
        - Should have at least one uppercase character
        - Should have at least one numeric character
        - Should have at least one special character (@$!%*#?&)

    Args:
        password (str): The string to check.

    Raises:
        ValueError: Raises when password is not valid.

    Returns:
        str: The password if it is valid.
    """
    match = re.search(
        r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])" r"[A-Za-z\d@$!#%*?&]{8,32}$",
        password,
    )
    if not match:
        raise ValueError("Invalid password")

    return password
