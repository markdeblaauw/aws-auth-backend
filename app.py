#!/usr/bin/env python3
"""AWS CDK entry point."""
import os

import aws_cdk as cdk
from dotenv import load_dotenv

from deployment import Backend

app = cdk.App()

application_name = app.node.try_get_context("application_name")
stage = app.node.try_get_context("stage")

# Load stage config
load_dotenv(f"config/{stage}.env")
env = cdk.Environment(account=os.environ["CDK_DEFAULT_ACCOUNT"], region=os.getenv("CDK_REGION"))

Backend(app, f"{application_name}-{stage}", env=env)

app.synth()
