"""Cognito authentication integration."""
import aws_cdk as cdk
import constructs
from aws_cdk import aws_cognito as cognito


class Authentication(constructs.Construct):
    """Authentication class."""

    def __init__(self, scope: constructs.Construct, id: str, application_name: str, stage: str) -> None:
        """Initialises the class with additional parameters.

        Args:
            scope (constructs.Construct): Parent construct.
            id (str): Identifier.
            application_name (str): The name of the application in which this construct
                is build.
            stage (str): The stage in which this construct is part of. This can be either
                dev, test or prod.
        """
        super().__init__(scope, id)

        self.pool = cognito.UserPool(
            self,
            "AuthenticationPool",
            account_recovery=cognito.AccountRecovery.EMAIL_ONLY,
            sign_in_aliases=cognito.SignInAliases(email=True),
            enable_sms_role=False,
            self_sign_up_enabled=True,
            sign_in_case_sensitive=False,
            user_pool_name=f"{application_name}-cognito-{stage}",
        )

        self.client = self.pool.add_client(
            "AuthenticationClient",
            auth_flows=cognito.AuthFlow(admin_user_password=True),
            access_token_validity=cdk.Duration.minutes(60),
            id_token_validity=cdk.Duration.minutes(60),
            refresh_token_validity=cdk.Duration.minutes(60),
            generate_secret=True,
            prevent_user_existence_errors=True,
            user_pool_client_name=f"{application_name}-cognito-{stage}",
        )

        # CloudFormation stack outputs.
        cdk.CfnOutput(
            self,
            "UserPoolId",
            value=self.user_pool_id,
            export_name=f"{application_name}-user-pool-id-{stage}",
            description="The Congito app Id.",
        )

        cdk.CfnOutput(
            self,
            "UserPoolClientId",
            value=self.user_pool_client_id,
            export_name=f"{application_name}-user-pool-client-id-{stage}",
            description="The Cognito app client Id.",
        )

    @property
    def user_pool_id(self) -> str:
        """Get the user pool id.

        Returns:
            str: The user pool id.
        """
        return self.pool.user_pool_id

    @property
    def user_pool_arn(self) -> str:
        """Get the user pool arn.

        Returns:
            str: The user pool arn.
        """
        return self.pool.user_pool_arn

    @property
    def user_pool_client_id(self) -> str:
        """Get the user pool client id.

        Returns:
            str: The user pool client id.
        """
        return self.client.user_pool_client_id
