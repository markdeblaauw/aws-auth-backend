# aws-auth-backend

This repository includes code and infrastructure templates to create a serverless authentication service. This is done with AWS services with, specifically, AWS Cognito. You can find the following information in this README:

- Visual description template.
- Install the service locally and on AWS.

## Visual description template

![Alt text](docs/serverless_authentication.png?raw=true "Title")

## Install the service locally and on AWS.

### Set-up repository locally.
This is written for a Linux or MacOs environment.

Local requirements:
- AWS CLI 2
- Python 3.8
- NodeJs
- make
- bash
- git

### Deploy service on AWS.


###
