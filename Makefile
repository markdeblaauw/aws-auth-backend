# Variable setting, e.g., make synth STAGE=prod
STAGE?=dev

.PHONY: tests coverage synth deploy diff destroy

install_cdk:
	npm ci

local_venv:
	python3 -m venv venv

install_packages:
	python -m pip install --upgrade pip
	pip install pip-tools==6.5.1

	pip-sync secret/runtime/cf_secret_maker/requirements.txt \
		requirements.txt requirements-dev.txt

	pre-commit install

compile_python_requirements:
	pip-compile secret/runtime/cf_secret_maker/requirements.in
	pip-compile requirements.in
	pip-compile requirements-dev.in

pre_commit:
	pre-commit run --all-files

tests:
	. config/paths.sh && \
	pytest tests/*

coverage_report:
	. config/paths.sh && \
	coverage run -m pytest
	coverage html --skip-covered
	open htmlcov/index.html

# e.g., `make synth STAGE=prod`
synth:
	@npx cdk synth -c stage=$(STAGE)

deploy: synth
	@npx cdk deploy aws-authentication-$(STAGE)/* -c stage=$(STAGE) --require-approval never

diff:
	@npx cdk diff -c stage=$(STAGE)

destroy:
	@npx cdk destroy -c stage=$(STAGE)

bootstrapp-cdk-toolkit:
	# Get deployment region from env file.
	# Then get Account id.
	export $(shell cat config/$(STAGE).env | xargs); \
	ACCOUNT=$(shell aws sts get-caller-identity --query Account --output text); \
	npx cdk bootstrap $$ACCOUNT/$$CDK_REGION -c stage=$(STAGE)