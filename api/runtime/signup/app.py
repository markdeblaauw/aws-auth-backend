"""Lambda code handler for login."""
import json
import logging
import os

import boto3
from aws_lambda_powertools.utilities.data_classes import APIGatewayProxyEventV2
from aws_lambda_powertools.utilities.parser import BaseModel, ValidationError, parse, validator
from lambda_utils import APIResponseCodes, api_response, secret_cognito_hash
from lambda_utils.validators import password_validator
from pydantic import EmailStr


class Credentials(BaseModel):
    """Event body request class."""

    username: EmailStr
    password: str

    @validator("password")
    def check_password(cls, value: str) -> str:
        """Check if password is valid.

        Args:
            value (str): The password to check.

        Returns:
            str: Password if it is valid.
        """
        return password_validator(password=value)


def handler(event: dict, context: dict) -> dict:
    """Event handler to process incoming requests for signing up.

    Args:
        event (dict): Request information from the client.
        context (dict): Runtime information from the Lambda
            function.

    Returns:
        dict: Response of the request.
    """
    parsed_event = APIGatewayProxyEventV2(event)
    try:
        parsed_body: Credentials = parse(event=parsed_event.body, model=Credentials)
    except ValidationError as e:
        for an_error in e.errors():
            if an_error["msg"] == "Invalid password":
                return api_response(
                    status_code=APIResponseCodes.BAD_REQUEST,
                    body={"message": "Invalid password"},
                    headers={
                        "content-type": "application/json",
                        "Cache-Control": "no-cache",
                    },
                )
            elif an_error["type"] == "value_error.email":
                return api_response(
                    status_code=APIResponseCodes.BAD_REQUEST,
                    body={"message": "Invalid email"},
                    headers={
                        "content-type": "application/json",
                        "Cache-Control": "no-cache",
                    },
                )
        return api_response(
            status_code=APIResponseCodes.BAD_REQUEST,
            body={"message": "Invalid request"},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )

    return signup(username=parsed_body.username, password=parsed_body.password)


def signup(username: str, password: str) -> dict:
    """Signs up a user into a Cognito pool.

    Args:
        username (str): The username of the client.
        password (str): The password of the client.

    Returns:
        dict: Response on the request.
    """
    secret_client = boto3.client("secretsmanager")
    cognito_client = boto3.client("cognito-idp")

    # Retrieve the Cognito secret for hash secret
    try:
        cognito_secret_string = secret_client.get_secret_value(SecretId=os.environ["secret_name"])["SecretString"]
    except Exception as e:
        logging.error(e)
        return api_response(
            status_code=APIResponseCodes.INTERNAL_SERVER_ERROR,
            body={"message": "An internal error has occurered"},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )

    cognito_secret = json.loads(cognito_secret_string)

    try:
        cognito_client.sign_up(
            ClientId=os.environ["client_id"],
            SecretHash=secret_cognito_hash(
                username=username,
                cognito_client_id=os.environ["client_id"],
                cognito_secret=cognito_secret["clientSecret"],
            ),
            Username=username,
            Password=password,
        )
    except cognito_client.exceptions.UsernameExistsException:
        return api_response(
            status_code=APIResponseCodes.BAD_REQUEST,
            body={"message": "This username already exists"},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )
    except cognito_client.exceptions.InvalidPasswordException:
        return api_response(
            status_code=APIResponseCodes.BAD_REQUEST,
            body={"message": "Invalid password"},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )
    except cognito_client.exceptions.UserLambdaValidationException:
        return api_response(
            status_code=APIResponseCodes.BAD_REQUEST,
            body={"message": "This email already exists"},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )
    except Exception as e:
        logging.error(e)
        api_response(
            status_code=APIResponseCodes.BAD_REQUEST,
            body={"message": str(e)},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )
    return api_response(
        status_code=APIResponseCodes.OK,
        body={"message": "signup succesful"},
        headers={
            "content-type": "application/json",
            "Cache-Control": "no-cache",
        },
    )
