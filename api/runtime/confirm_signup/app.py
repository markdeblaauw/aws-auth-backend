"""Contains logic for signup confirmation."""
import json
import logging
import os

import boto3
from aws_lambda_powertools.utilities.data_classes import APIGatewayProxyEventV2
from aws_lambda_powertools.utilities.parser import BaseModel, ValidationError, parse
from lambda_utils import APIResponseCodes, api_response, secret_cognito_hash
from pydantic import EmailStr


class Credentials(BaseModel):
    """Event body request class."""

    username: EmailStr
    confirmation_code: str


def handler(event: dict, context: dict) -> dict:
    """Event handler to process incoming requests for signup confirmation.

    Args:
        event (dict): Request information from the client.
        context (dict):Runtime information from the Lambda
            function.

    Returns:
        dict: Response of the request.
    """
    parsed_event = APIGatewayProxyEventV2(event)
    try:
        parsed_body: Credentials = parse(event=parsed_event.body, model=Credentials)
    except ValidationError as e:
        for an_error in e.errors():
            if an_error["type"] == "value_error.email":
                return api_response(
                    status_code=APIResponseCodes.BAD_REQUEST,
                    body={"message": "Invalid email"},
                    headers={
                        "content-type": "application/json",
                        "Cache-Control": "no-cache",
                    },
                )
        return api_response(
            status_code=APIResponseCodes.BAD_REQUEST,
            body={"message": "Invalid request"},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )

    return confirm_signup(
        username=parsed_body.username,
        confirmation_code=parsed_body.confirmation_code,
    )


def confirm_signup(username: str, confirmation_code: str) -> dict:
    """Confirms user's username (email) with given confirmation code.

    Args:
        username (str): User's username to confirm.
        confirmation_code (str): The confirmation code to
            confirm signup with.

    Returns:
        dict: Response to the request.
    """
    secret_client = boto3.client("secretsmanager")
    cognito_client = boto3.client("cognito-idp")

    # Retrieve the Cognito secret for hash secret
    try:
        cognito_secret_string = secret_client.get_secret_value(
            SecretId=os.environ["secret_name"],
        )["SecretString"]
    except Exception as e:
        logging.error(e)
        return api_response(
            status_code=APIResponseCodes.INTERNAL_SERVER_ERROR,
            body={"message": "An internal error has occurered"},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )

    cognito_secret = json.loads(cognito_secret_string)

    try:
        cognito_client.confirm_sign_up(
            ClientId=os.environ["client_id"],
            SecretHash=secret_cognito_hash(
                username=username,
                cognito_client_id=os.environ["client_id"],
                cognito_secret=cognito_secret["clientSecret"],
            ),
            Username=username,
            ConfirmationCode=confirmation_code,
            ForceAliasCreation=False,
        )
    except cognito_client.exceptions.UserNotFoundException:
        return api_response(
            status_code=APIResponseCodes.BAD_REQUEST,
            body={"message": "Username does not exists"},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )
    except cognito_client.exceptions.CodeMismatchException:
        return api_response(
            status_code=APIResponseCodes.BAD_REQUEST,
            body={"message": "Invalid verification code"},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )
    except cognito_client.exceptions.NotAuthorizedException:
        return api_response(
            status_code=APIResponseCodes.BAD_REQUEST,
            body={"message": "User is already confirmed"},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )
    except Exception as e:
        logging.error(e)
        api_response(
            status_code=APIResponseCodes.BAD_REQUEST,
            body={"message": str(e)},
            headers={
                "content-type": "application/json",
                "Cache-Control": "no-cache",
            },
        )
    return api_response(
        status_code=APIResponseCodes.OK,
        body={"message": "confirmation succesful"},
        headers={
            "content-type": "application/json",
            "Cache-Control": "no-cache",
        },
    )
