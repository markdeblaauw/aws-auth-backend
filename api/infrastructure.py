"""Logic to create an API."""
import os

import aws_cdk as cdk
import constructs
from aws_cdk import aws_apigatewayv2_alpha as apigatewayv2
from aws_cdk import aws_apigatewayv2_integrations_alpha as apigatewayv2_integration
from aws_cdk import aws_iam as iam
from aws_cdk import aws_lambda as _lambda


class API(constructs.Construct):
    """API class that includes API Gateway and Lambda integrations."""

    def __init__(
        self,
        scope: constructs.Construct,
        id: str,
        application_name: str,
        stage: str,
        authentication: constructs.Construct,
        cognito_secret: constructs.Construct,
        utils_lambda_layer: _lambda.ILayerVersion,
        powertools_layer: _lambda.ILayerVersion,
    ) -> None:
        """Initialises the class with additional parameters.

        Args:
            scope (constructs.Construct): Parent construct.
            id (str): Identifier.
            application_name (str): The name of the application in which this construct
                is build.
            stage (str): The stage in which this construct is part of. This can be either
                dev, test or prod.
            authentication (constructs.Construct): Authentication construct that contains AWS Cognito.
            cognito_secret (constructs.Construct): Secret construct that contains AWS secret manager.
            utils_lambda_layer (_lambda.ILayerVersion): A utility Lambda layer.
            powertools_layer (_lambda.ILayerVersion): A Powertools Lambda layer.
        """
        super().__init__(scope, id)
        self.application_name = application_name
        self.stage = stage

        self.http_api = apigatewayv2.HttpApi(
            self,
            "ApplicationApi",
            api_name=f"{application_name}-http-api-{stage}",
            description=f"{application_name} api.",
        )

        # CloudFormation stack api url output.
        cdk.CfnOutput(
            self,
            "ApiUrlOutput",
            value=self.http_api.url,
            export_name=f"{application_name}-api-url-{stage}",
            description="The url of the api.",
        )

        # Image api / lambda integration.
        signup_lambda = self._create_signup_lambda(
            cognito_secret=cognito_secret,
            authentication=authentication,
            utils_layer=utils_lambda_layer,
            powertools_layer=powertools_layer,
        )

        self.http_api.add_routes(
            path="/api/signup",
            methods=[apigatewayv2.HttpMethod.POST],
            integration=apigatewayv2_integration.HttpLambdaIntegration("SignupIntegration", handler=signup_lambda),
        )

        login_lambda = self._create_login_lambda(
            cognito_secret=cognito_secret,
            authentication=authentication,
            utils_layer=utils_lambda_layer,
            powertools_layer=powertools_layer,
        )

        self.http_api.add_routes(
            path="/api/login",
            methods=[apigatewayv2.HttpMethod.POST],
            integration=apigatewayv2_integration.HttpLambdaIntegration("LoginIntegration", handler=login_lambda),
        )

        confirm_signup_lambda = self._create_confirm_signup_lambda(
            cognito_secret=cognito_secret,
            authentication=authentication,
            utils_layer=utils_lambda_layer,
            powertools_layer=powertools_layer,
        )

        self.http_api.add_routes(
            path="/api/confirm_signup",
            methods=[apigatewayv2.HttpMethod.POST],
            integration=apigatewayv2_integration.HttpLambdaIntegration(
                "ConfirmSignupIntegration", handler=confirm_signup_lambda
            ),
        )

    def _create_signup_lambda(
        self,
        cognito_secret: constructs.Construct,
        authentication: constructs.Construct,
        utils_layer: _lambda.ILayerVersion,
        powertools_layer: _lambda.ILayerVersion,
    ) -> _lambda.IFunction:
        """Creation of the signup Lambda function.

        Args:
            cognito_secret (constructs.Construct): Secret construct that contains AWS secret manager.
            authentication (constructs.Construct): Authentication construct that contains AWS Cognito.
            utils_layer (_lambda.ILayerVersion): A utility Lambda layer.
            powertools_layer (_lambda.ILayerVersion): A Powertools Lambda layer.

        Returns:
            _lambda.IFunction: The signup Lambda function.
        """
        signup_lambda_role = iam.Role(
            self,
            "SignupLambdaRole",
            assumed_by=iam.ServicePrincipal("lambda.amazonaws.com"),
            managed_policies=[
                iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AWSLambdaBasicExecutionRole")
            ],
        )

        signup_lambda_role.add_to_policy(
            statement=iam.PolicyStatement(
                actions=["secretsmanager:GetSecretValue"],
                resources=[cognito_secret.secret_arn],
                effect=iam.Effect.ALLOW,
            )
        )

        signup_lambda_role.add_to_policy(
            statement=iam.PolicyStatement(
                actions=["cognito-idp:SignUp"], resources=[authentication.user_pool_arn], effect=iam.Effect.ALLOW
            )
        )

        signup_lambda_function = _lambda.Function(
            self,
            "SignupFunction",
            code=_lambda.Code.from_asset(path=os.path.join(os.path.dirname(__file__), "runtime", "signup")),
            handler="app.handler",
            runtime=_lambda.Runtime.PYTHON_3_8,
            environment={"secret_name": cognito_secret.secret_name, "client_id": authentication.user_pool_client_id},
            layers=[utils_layer, powertools_layer],
            timeout=cdk.Duration.seconds(10),
            memory_size=512,
            role=signup_lambda_role,
        )

        return signup_lambda_function

    def _create_login_lambda(
        self,
        cognito_secret: constructs.Construct,
        authentication: constructs.Construct,
        utils_layer: _lambda.ILayerVersion,
        powertools_layer: _lambda.ILayerVersion,
    ) -> _lambda.IFunction:
        """Creation of the login Lambda function.

        Args:
            cognito_secret (constructs.Construct): Secret construct that contains AWS secret manager.
            authentication (constructs.Construct): Authentication construct that contains AWS Cognito.
            utils_layer (_lambda.ILayerVersion): A utility Lambda layer.
            powertools_layer (_lambda.ILayerVersion): A Powertools Lambda layer.

        Returns:
            _lambda.IFunction:  The login Lambda function.
        """
        login_lambda_role = iam.Role(
            self,
            "LoginLambdaRole",
            assumed_by=iam.ServicePrincipal("lambda.amazonaws.com"),
            managed_policies=[
                iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AWSLambdaBasicExecutionRole")
            ],
        )

        login_lambda_role.add_to_policy(
            statement=iam.PolicyStatement(
                actions=["secretsmanager:GetSecretValue"],
                resources=[cognito_secret.secret_arn],
                effect=iam.Effect.ALLOW,
            )
        )

        login_lambda_role.add_to_policy(
            statement=iam.PolicyStatement(
                actions=["cognito-idp:AdminInitiateAuth"],
                resources=[authentication.user_pool_arn],
                effect=iam.Effect.ALLOW,
            )
        )

        login_lambda_function = _lambda.Function(
            self,
            "LoginFunction",
            code=_lambda.Code.from_asset(path=os.path.join(os.path.dirname(__file__), "runtime", "login")),
            handler="app.handler",
            runtime=_lambda.Runtime.PYTHON_3_8,
            environment={"secret_name": cognito_secret.secret_name, "client_id": authentication.user_pool_client_id},
            layers=[utils_layer, powertools_layer],
            timeout=cdk.Duration.seconds(10),
            memory_size=512,
            role=login_lambda_role,
        )

        return login_lambda_function

    def _create_confirm_signup_lambda(
        self,
        cognito_secret: constructs.Construct,
        authentication: constructs.Construct,
        utils_layer: _lambda.ILayerVersion,
        powertools_layer: _lambda.ILayerVersion,
    ) -> _lambda.IFunction:
        """Creation of the confirm signup Lambda function.

        Args:
            cognito_secret (constructs.Construct): Secret construct that contains AWS secret manager.
            authentication (constructs.Construct): Authentication construct that contains AWS Cognito.
            utils_layer (_lambda.ILayerVersion): A utility Lambda layer.
            powertools_layer (_lambda.ILayerVersion): A Powertools Lambda layer.

        Returns:
            _lambda.IFunction:  The confirm signup Lambda function.
        """
        confirm_signup_lambda_role = iam.Role(
            self,
            "ConfirmSignupLambdaRole",
            assumed_by=iam.ServicePrincipal("lambda.amazonaws.com"),
            managed_policies=[
                iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AWSLambdaBasicExecutionRole")
            ],
        )

        confirm_signup_lambda_role.add_to_policy(
            statement=iam.PolicyStatement(
                actions=["secretsmanager:GetSecretValue"],
                resources=[cognito_secret.secret_arn],
                effect=iam.Effect.ALLOW,
            )
        )

        confirm_signup_lambda_role.add_to_policy(
            statement=iam.PolicyStatement(
                actions=["cognito-idp:SignUp"], resources=[authentication.user_pool_arn], effect=iam.Effect.ALLOW
            )
        )

        confirm_signup_lambda_function = _lambda.Function(
            self,
            "ConfirmSignupFunction",
            code=_lambda.Code.from_asset(path=os.path.join(os.path.dirname(__file__), "runtime", "confirm_signup")),
            handler="app.handler",
            runtime=_lambda.Runtime.PYTHON_3_8,
            environment={"secret_name": cognito_secret.secret_name, "client_id": authentication.user_pool_client_id},
            layers=[utils_layer, powertools_layer],
            timeout=cdk.Duration.seconds(10),
            memory_size=512,
            role=confirm_signup_lambda_role,
        )

        return confirm_signup_lambda_function
