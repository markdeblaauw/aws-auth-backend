"""Create secret for Cognito key."""
import os

import aws_cdk as cdk
import constructs
from aws_cdk import aws_iam as iam
from aws_cdk import aws_lambda as _lambda


class CognitoSecret(constructs.Construct):
    """Secret class."""

    def __init__(
        self,
        scope: constructs.Construct,
        id: str,
        application_name: str,
        stage: str,
        cognito_pool_id: str,
        cognito_client_pool_id: str,
    ) -> None:
        """Initialises the class with additional parameters.

        Args:
            scope (constructs.Construct): Parent construct.
            id (str): Identifier.
            application_name (str): The name of the application in which this construct
                is build.
            stage (str): The stage in which this construct is part of. This can be either
                dev, test or prod.
            cognito_pool_id (str): The id of the AWS Cognito pool.
            cognito_client_pool_id (str): The id of the client pool from the Cognito pool.
        """
        super().__init__(scope, id)

        lambda_role = iam.Role(
            self,
            "LambdaRole",
            assumed_by=iam.ServicePrincipal("lambda.amazonaws.com"),
            managed_policies=[
                iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AWSLambdaBasicExecutionRole")
            ],
        )

        lambda_role.add_to_policy(
            statement=iam.PolicyStatement(
                actions=[
                    "cognito-idp:DescribeUserPoolClient",
                    "secretsmanager:CreateSecret",
                    "secretsmanager:TagResource",
                    "secretsmanager:UpdateSecret",
                    "secretsmanager:DeleteSecret",
                ],
                resources=["*"],
                effect=iam.Effect.ALLOW,
            )
        )

        secret_lambda_maker = _lambda.Function(
            self,
            "AWSSecretMakerLambda",
            code=_lambda.Code.from_asset(
                path=os.path.join(os.path.dirname(__file__), "runtime", "cf_secret_maker"),
                bundling=cdk.BundlingOptions(
                    image=cdk.DockerImage.from_build(path=os.path.join(os.path.dirname(__file__)), file="Dockerfile"),
                    command=["cp", "-R", "/asset-stage/.", "/asset-output"],
                ),
            ),
            handler="cf_secret_maker.app.handler",
            runtime=_lambda.Runtime.PYTHON_3_8,
            timeout=cdk.Duration.minutes(1),
            role=lambda_role,
        )

        self.secret_custom_resource = cdk.CustomResource(
            self,
            "CustomResource",
            resource_type="Custom::UserPoolClientSecret",
            service_token=secret_lambda_maker.function_arn,
            properties={"UserPoolId": cognito_pool_id, "AppClientId": cognito_client_pool_id},
        )

    @property
    def secret_name(self) -> str:
        """The secret name of the secret."""
        return self.secret_custom_resource.get_att_string("SecretName")

    @property
    def secret_arn(self) -> str:
        """The arn of the secret."""
        return self.secret_custom_resource.get_att_string("SecretArn")
